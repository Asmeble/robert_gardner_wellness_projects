FROM  mltframework/melt:v7.0.1
ADD test_sh.sh .
ADD editor_of_5_inputs.sh .
RUN apt-get update && echo msttcorefonts msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections && apt-get install -fy python2 wget --reinstall ttf-mscorefonts-installer && wget https://bootstrap.pypa.io/pip/2.7/get-pip.py && python2 get-pip.py && rm get-pip.py && python2 -m pip install moviepy && apt-get remove -y wget && mv editor_of_5_inputs.sh vid_editor.sh && sed -i 's/\r//' vid_editor.sh && sed -i 's/\r//' test_sh.sh && sed -i -e 's/python/python2/g' test_sh.sh && sed -i -e 's/0 1 26/(0, 1, 26)/g' test_sh.sh && mv test_sh.sh vid_chopper.sh && mkdir vid_folder/
WORKDIR /mnt/vid_folder/
ENTRYPOINT ["bash"]