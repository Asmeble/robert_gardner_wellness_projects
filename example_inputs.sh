#!/usr/bin/env bash
file_to_chop=$1; intro=$2; text="$3"; segments_dir=$4; outro=$5
#create empty file list
file_list="" # for every file in listed directory:
for files in $segments_dir/*.mp4;
do #add file into the file list
	file_list+=" $files";
done
#                                                Chop the video & put in the listed directory. Then add back together including custom intro & outro
/mnt/vid_chopper.sh $file_to_chop $segments_dir; melt $intro -attach dynamictext:"$text" geometry="00:00:00.000=-564 603 941 121 1;00:00:03.017=518 612 201 105 1"  family="comic sans ms"  $file_list $outro -consumer avformat:$segments_dir/output/combined_$file_to_chop acodec=libmp3lame vcodec=libx264
#                                                                                      set geometry with start and end times to designate play speed and position