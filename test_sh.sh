#!/usr/bin/env bash
time_adjust(){ #adjust the time backwards, & forwards
	hrs=$(echo $1| awk '{split($0,a,":"); print a[1]}'); mins=$(echo $1| awk '{split($0,a,":"); print a[2]}'); secs=$(echo $1| awk '{split($0,a,":"); print a[3]}')
	python -c """
import datetime
t=datetime.time(hour=int(\""""$hrs"""\"), minute=int(\""""$mins"""\"),second=int(\""""$secs"""\"))
hh,mm,ss=str(t).split(\":\")
hh,mm,ss=int(hh),int(mm),int(ss)
print(t.replace(hour=hh+"""$2""", minute=mm+"""$3""", second=ss+"""$4"""))
"""
}
# did we reach the end of the video
end_vid_length_check(){
	hrs_1=$(echo $1| awk '{split($0,a,":"); print a[1]}'); mins_1=$(echo $1| awk '{split($0,a,":"); print a[2]}'); secs_1=$(echo $1| awk '{split($0,a,":"); print a[3]}')
	hrs_2=$(echo $2| awk '{split($0,a,":"); print a[1]}'); mins_2=$(echo $2| awk '{split($0,a,":"); print a[2]}'); secs_2=$(echo $2| awk '{split($0,a,":"); print a[3]}')
	python -c "print(int(\""$hrs_1"\")-int(\""$hrs_2"\"), int(\""$mins_1"\")-int(\""$mins_2"\"), int(\""$secs_1"\")-int(\""$secs_2"\"))"	
}
vid_start="00:00:00:00" #set the beginning time
filename=$1 #get filename as first parameter
vid_length=$(python -c "from moviepy.editor import VideoFileClip; from datetime import timedelta; clip = VideoFileClip(\"$filename\"); print( timedelta(seconds=int(clip.duration))); clip.close()") #find end of video
adjusted_in=$(time_adjust $vid_start 0 0 0); adjusted_out=$(time_adjust $vid_start 0 0 8) #adjust the start and stop times for the intro
### melt $filename in="$adjusted_in:00" out="$adjusted_out:00" -consumer avformat:$2/intro.mp4 acodec=libmp3lame vcodec=libx264 #then makes the intro
VAR1="0 1 26" # the time left for the ad at the end of the video
i=0 #set the segment for the video pieces to 0
while [[ "$VAR1" != "$(end_vid_length_check $vid_length $(time_adjust $adjusted_out 0 -1 0):00)" ]]; do # if the time left that is not equal to the difference of the video length and adjusted output timestamp minus a minute, loop
	adjusted_in=$(time_adjust $adjusted_out 0 0 0); adjusted_out=$(time_adjust $adjusted_in 0 5 0) #adjust the input and output
	if [[ "$VAR1" == "$(end_vid_length_check $vid_length $(time_adjust $adjusted_out 0 -1 0):00)" ]]; then #if the time left that is equal to the difference of the video length and adjusted output timestamp minus a minute
		adjusted_out1=$(time_adjust $adjusted_out 0 -1 17) #adjust the time out #seconds should be adjusted for 17 seconds isntead of 0
		melt $filename in="$adjusted_in:00" out="$adjusted_out1:00" acodec=libmp3lame vcodec=libx264 -consumer avformat:$2/test_$i.mp4 acodec=libmp3lame vcodec=libx264 #make the second to last clip
		adjusted_in=$(time_adjust $adjusted_out 0 -1 0); adjusted_out=$(time_adjust $vid_length 0 0 0) #ajust the time in and out
		(( i = $i + 1 )) #increment the clip number by one
		### melt $filename in="$adjusted_in:00" out="$adjusted_out:00" -consumer avformat:test_out/sub_dir/test_outro.mp4 acodec=libmp3lame vcodec=libx264 #then make the outro and break the loop
		break
	fi
	melt $filename in="$adjusted_in:00" out="$adjusted_out:00" -consumer avformat:$2/test_$i.mp4 acodec=libmp3lame vcodec=libx264 #else, keep chopping the video as normal and increment the clip number
	(( i = $i + 1 )) #increment the clip number by one
done